import { TestBed } from '@angular/core/testing';

import { LighthouseService } from './lighthouse.service';

describe('LighthouseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LighthouseService = TestBed.get(LighthouseService);
    expect(service).toBeTruthy();
  });
});
