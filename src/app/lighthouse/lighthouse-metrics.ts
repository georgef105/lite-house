
export const LIGHTHOUSE_METRIC_NAMES = [
  'first-contentful-paint',
  'first-meaningful-paint',
  'speed-index',
  'first-cpu-idle',
  'time-to-interactive'
] as const;

export type LighthouseMetricNames = typeof LIGHTHOUSE_METRIC_NAMES[number];

export interface LighthouseMetricConfig {
  name: LighthouseMetricNames;
  displayName: string;
  weighting: number;
  median: number;
  falloff: number;
}

export const LIGHTHOUSE_METRIC_CONFIGS: Array<LighthouseMetricConfig> = [
  {
    name: 'first-contentful-paint',
    displayName: 'First Contentful Paint',
    weighting: 0.2, // ??
    median: 4000, // ?
    falloff: 2000 // ?
  },
  {
    name: 'first-meaningful-paint',
    displayName: 'First Meaningful Paint',
    weighting: 0.067, // ??
    median: 4000, // ?
    falloff: 2000 // ?
  },
  {
    name: 'speed-index',
    displayName: 'Speed Index',
    weighting: 0.267, // ??
    median: 5800, // ?
    falloff: 2900 // ?
  },
  {
    name: 'time-to-interactive',
    displayName: 'Time to Interactive',
    weighting: 0.333, // ??
    median: 7300, // ?
    falloff: 2900 // ?
  },
  {
    name: 'first-cpu-idle',
    displayName: 'First CPU Idle',
    weighting: 0.133, // ??
    median: 6500, // ?
    falloff: 2900 // ?
  }
];

export type LighthouseMetricValues = Record<LighthouseMetricNames, number>;

export interface LighthouseMetricScore extends LighthouseMetricConfig {
  value: number;
  score: number;
}

export interface LighthouseScore {
  totalScore: number;
  metrics: Array<LighthouseMetricScore>;
}
