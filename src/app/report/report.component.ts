import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { LighthouseMetricNames, LighthouseMetricValues, LighthouseScore } from '../lighthouse/lighthouse-metrics';

import { map } from 'rxjs/operators';
import { LighthouseService } from '../lighthouse/lighthouse.service';

const QUERY_PARAM_TO_METRIC_NAME_MAP: Record<string, LighthouseMetricNames> = {
  'fcp': 'first-contentful-paint',
  'fmp': 'first-meaningful-paint',
  'si': 'speed-index',
  'cpu': 'first-cpu-idle',
  'ti': 'time-to-interactive'
};

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  public metricValues$: Observable<LighthouseMetricValues>;

  public lighthouseScore$: Observable<LighthouseScore>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private lighthouseService: LighthouseService
  ) { }

  ngOnInit() {
    this.metricValues$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => {
        return Object.keys(QUERY_PARAM_TO_METRIC_NAME_MAP).reduce((metricValues, key) => {
          metricValues[QUERY_PARAM_TO_METRIC_NAME_MAP[key]] = queryParams[key];
          return metricValues;
        }, {} as LighthouseMetricValues);
      })
    );

    this.lighthouseScore$ = this.metricValues$.pipe(
      map(values => this.lighthouseService.getScore(values))
    );
  }

}
